# Base



## Descrição:

O objetivo do projeto "Base" é fornecer os arquivos de configuração para servirem de base para outros projetos.

A pasta '.vscode' possui o arquivo 'settings.json' com as configurações para o VsCode.
O arquivo 'config.sh' é um script para configurar os parâmetros de usuário e de e-mail no gitlab, bem como instalar extensões para o VsCode.

Preencha corretamente as linhas:
'''
  git config --global user.name "seu_user_name_no_gitlab"
  git config --global user.email "seu_email_no_gitlab"
'''
com o seu nome de usuário e o seu email, respectivamnte.

Após clonar o projeto "Base", é preciso verificar se o script 'config.sh' possui permissão de execução. Caso contrário é preciso executar, no terminal de comandos, "chmod a+x config.sh". Após isso, executar o script com "./config.sh".

## Criação de novo projeto:

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/progaut/base.git
git branch -M main
git push -uf origin main
```
## Visuals


## Installation


## Usage


## Support


## Roadmap


## Contributing


## Authors and acknowledgment


## License


## Project status

